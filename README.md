# microservices-fun 

The goal of this project is to gain experience in microservice architect.

## Аrchitecture

Not yet :)

### [spotify-get](spotify-get/README.md)

The first service. You cas use it to get information about tracks, albums, artists and playlists using the web api.  

## Run
0. You need `docker` and `docker-compose`
1. You must provide `.env` file. See [.env-example](.env-example) file.
2. `docker-compose up --build`

package client

import (
	"context"
	"log"
	"spotify-get/api"

	"github.com/zmb3/spotify/v2"
	spotifyauth "github.com/zmb3/spotify/v2/auth"
	"golang.org/x/oauth2/clientcredentials"

	"spotify-get/global"
)

type SpotifyClient struct {
	auth         *spotifyauth.Authenticator
	globalClient *spotify.Client
	ctx          context.Context
}

func New(config *global.Config) *SpotifyClient {
	res := &SpotifyClient{
		ctx: context.Background(),
	}

	creds := &clientcredentials.Config{
		ClientID:     config.SpotifyID,
		ClientSecret: config.SpotifySecret,
		TokenURL:     spotifyauth.TokenURL,
	}

	token, err := creds.Token(res.ctx)
	if err != nil {
		log.Fatalf("Couldn't get token: %v", err)
	}

	res.auth = spotifyauth.New()
	res.globalClient = spotify.New(res.auth.Client(res.ctx, token))

	return res
}

func (c *SpotifyClient) GetTrackInfo(id string) (*api.TrackInfo, error) {
	track, err := c.globalClient.GetTrack(c.ctx, spotify.ID(id))
	if err != nil {
		return nil, err
	}

	return &api.TrackInfo{
		TrackShortInfo: api.TrackShortInfo{
			URI:  string(track.URI),
			Name: track.Name,
		},
		OpenLink: track.ExternalURLs["spotify"],
		Album:    track.Album.Name,
		AlbumURI: string(track.Album.URI),
		Artists:  makeArtistShortInfo(track.Artists),
	}, nil
}

func (c *SpotifyClient) GetAlbumInfo(id string) (*api.AlbumInfo, error) {
	album, err := c.globalClient.GetAlbum(c.ctx, spotify.ID(id))
	if err != nil {
		return nil, err
	}

	tracks := make([]api.TrackShortInfo, len(album.Tracks.Tracks))
	for i, track := range album.Tracks.Tracks {
		tracks[i].URI = string(track.URI)
		tracks[i].Name = track.Name
	}

	return &api.AlbumInfo{
		AlbumShortInfo: api.AlbumShortInfo{
			URI:  string(album.URI),
			Name: album.Name,
			Type: album.AlbumType,
		},
		OpenLink: album.ExternalURLs["spotify"],
		Artists:  makeArtistShortInfo(album.Artists),
		TotalTracksCount: album.Tracks.Total,
		Tracks:   tracks,
	}, nil
}

func (c *SpotifyClient) GetArtistInfo(id string) (*api.ArtistInfo, error) {
	artist, err := c.globalClient.GetArtist(c.ctx, spotify.ID(id))
	if err != nil {
		return nil, err
	}

	albums, err := c.globalClient.GetArtistAlbums(c.ctx, spotify.ID(id), nil)
	if err != nil {
		return nil, err
	}

	shortAlbums := make([]api.AlbumShortInfo, len(albums.Albums))
	for i, album := range albums.Albums {
		shortAlbums[i].URI = string(album.URI)
		shortAlbums[i].Name = album.Name
		shortAlbums[i].Type = album.AlbumType
	}

	return &api.ArtistInfo{
		ArtistShortInfo: api.ArtistShortInfo{
			URI:  string(artist.URI),
			Name: artist.Name,
		},
		Albums: shortAlbums,
	}, nil
}

func (c *SpotifyClient) GetPlaylistInfo(id string) (*api.PlaylistInfo, error) {
	playlist, err := c.globalClient.GetPlaylist(c.ctx, spotify.ID(id))
	if err != nil {
		return nil, err
	}

	count := min(20, len(playlist.Tracks.Tracks))
	tracks := make([]api.TrackInPlaylistInfo, count)
	for i := 0; i < count; i++ {
		tracks[i].URI = string(playlist.Tracks.Tracks[i].Track.URI)
		tracks[i].Name = playlist.Tracks.Tracks[i].Track.Name
		tracks[i].Artists = makeArtistShortInfo(playlist.Tracks.Tracks[i].Track.Artists)
	}

	return &api.PlaylistInfo{
		URI:         string(playlist.URI),
		Name:        playlist.Name,
		Description: playlist.Description,
		Owner: api.UserInfoShort{
			URI:         string(playlist.Owner.URI),
			DisplayName: playlist.Owner.DisplayName,
		},
		TotalTracksCount: playlist.Tracks.Total,
		Tracks: tracks,
	}, nil
}

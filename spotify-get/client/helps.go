package client

import (
	"github.com/zmb3/spotify/v2"

	"spotify-get/api"
)

func makeArtistShortInfo(artists []spotify.SimpleArtist) []api.ArtistShortInfo {
	res := make([]api.ArtistShortInfo, len(artists))
	for i, artist := range artists {
		res[i].URI = string(artist.URI)
		res[i].Name = artist.Name
	}
	return res
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

# spotify-get

The service for obtaining information about tracks, albums, artists and playlists on www.spotify.com

## Web API

The service is waiting for a connection on default port 40159.  
API is available:

## Dependencies

## Build
```sh
go build -o .build/service ./cmd/service
```

Or with Docker
```sh
docker build . -t spotify-get
```

## Run
Before you must provide environment variable:
- `CLIENT_ID` - ID of your application
- `CLIENT_SECRET` - Secret of your application
You can get them on this page: https://developer.spotify.com/dashboard/applications

Native run:
```sh
CLIENT_ID=someclientid CLIENT_SECRET=someclientsecret .build/service
```
Or in docker:
```sh
docker run --rm -p 40159 -ti -e CLIENT_ID=someclientid -e CLIENT_SECRET=someclientsecret spotify-get
```

Docker-compose is available [here](../README.md).

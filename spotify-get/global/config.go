package global

type Config struct {
	SpotifyID     string
	SpotifySecret string
}

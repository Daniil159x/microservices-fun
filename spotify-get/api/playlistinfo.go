package api

type TrackInPlaylistInfo struct {
	TrackShortInfo
	Artists []ArtistShortInfo `json:"artists"`
}

type PlaylistInfo struct {
	URI         string `json:"uri"`
	Name        string `json:"name"`
	Description string `json:"description"`

	Owner UserInfoShort `json:"owner"`

	TotalTracksCount int                   `json:"total_tracks_count"`
	Tracks           []TrackInPlaylistInfo `json:"tracks"`
}

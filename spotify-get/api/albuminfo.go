package api

type AlbumShortInfo struct {
	URI  string `json:"URI"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type AlbumInfo struct {
	AlbumShortInfo
	OpenLink string `json:"open_link"`

	Artists []ArtistShortInfo `json:"artists"`

	TotalTracksCount int              `json:"total_tracks_count"`
	Tracks           []TrackShortInfo `json:"tracks"`
}

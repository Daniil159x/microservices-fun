package api

type UserInfoShort struct {
	URI string `json:"uri"`
	DisplayName string `json:"display_name"`
}

package api

type TrackShortInfo struct {
	URI  string `json:"URI"`
	Name string `json:"name"`
}

type TrackInfo struct {
	TrackShortInfo
	OpenLink string `json:"open_link"`

	Album    string `json:"album"`
	AlbumURI string `json:"album_uri"`

	Artists []ArtistShortInfo `json:"artists"`
}

package api

type ArtistShortInfo struct {
	URI  string `json:"URI"`
	Name string `json:"name"`
}

type ArtistInfo struct {
	ArtistShortInfo

	Albums []AlbumShortInfo `json:"albums"`
}

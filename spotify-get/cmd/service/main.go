package main

import (
	"log"
	"os"
	"spotify-get/client"
	"spotify-get/global"
	"spotify-get/webapi"
)

func main() {

	cfg := global.Config{
		SpotifyID:     os.Getenv("CLIENT_ID"),
		SpotifySecret: os.Getenv("CLIENT_SECRET"),
	}

	spotify := client.New(&cfg)
	web := webapi.New(spotify)
	web.RegisterRoutes()
	log.Fatal(web.ListenAndServe("0.0.0.0", 40159))
}

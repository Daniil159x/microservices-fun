package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"spotify-get/client"
	"spotify-get/global"
)

func main() {

	cfg := global.Config{
		SpotifyID:     os.Getenv("CLIENT_ID"),
		SpotifySecret: os.Getenv("CLIENT_SECRET"),
	}

	spotify := client.New(&cfg)

	track, err := spotify.GetTrackInfo("4cOdK2wGLETKBW3PvgPWqT")
	if err != nil {
		log.Fatalf("couldn't get track: %v", err)
	}

	val, _ := json.MarshalIndent(track, "", "    ")
	fmt.Println(string(val))

	album, err := spotify.GetAlbumInfo("5Z9iiGl2FcIfa3BMiv6OIw")
	if err != nil {
		log.Fatalf("couldn't get album: %v", err)
	}

	val, _ = json.MarshalIndent(album, "", "    ")
	fmt.Println(string(val))

	artist, err := spotify.GetArtistInfo("0gxyHStUsqpMadRV0Di1Qt")
	if err != nil {
		log.Fatalf("couldn't get artist: %v", err)
	}

	val, _ = json.MarshalIndent(artist, "", "    ")
	fmt.Println(string(val))

	playlist, err := spotify.GetPlaylistInfo("541Iku0rHpxSUNWKe4KmIW")
	if err != nil {
		log.Fatalf("couldn't get playlist: %v", err)
	}

	val, _ = json.MarshalIndent(playlist, "", "    ")
	fmt.Println(string(val))
}

package webapi

import (
	"encoding/json"
	"net/http"
)

type HandlerWithID = func(id string) (data interface{}, err error)

func wrapID(handler HandlerWithID) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query()
		if !q.Has("id") {
			http.Error(w, "Argument 'id' not found", http.StatusBadRequest)
			return
		}

		data, err := handler(q.Get("id"))

		if err != nil {
			// TODO: ну это не правда, надо уметь различать ошибки
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		switch v := data.(type) {
		case []byte:
			w.Write(v)
		case string:
			w.Write([]byte(v))
		default:
			bytes, err := json.Marshal(data)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.Write(bytes)
		}
	}
}

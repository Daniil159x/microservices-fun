package webapi

import (
	"fmt"
	"net/http"
	"spotify-get/client"
)

type WebAPI struct {
	spotify *client.SpotifyClient
}

func New(client *client.SpotifyClient) *WebAPI {
	return &WebAPI{spotify: client}
}

func (api *WebAPI) ListenAndServe(ip string, port int) error {
	addr := fmt.Sprintf("%s:%d", ip, port)
	fmt.Printf("Start server on '%s'. API is awailable at http://%s/api\n", addr, addr)
	return http.ListenAndServe(addr, nil)
}

func (api *WebAPI) RegisterRoutes() {
	http.HandleFunc("/api/track", wrapID(func(id string) (interface{}, error) {
		return api.spotify.GetTrackInfo(id)
	}))

	http.HandleFunc("/api/album", wrapID(func(id string) (interface{}, error) {
		return api.spotify.GetAlbumInfo(id)
	}))

	http.HandleFunc("/api/artist", wrapID(func(id string) (interface{}, error) {
		return api.spotify.GetArtistInfo(id)
	}))

	http.HandleFunc("/api/playlist", wrapID(func(id string) (interface{}, error) {
		return api.spotify.GetPlaylistInfo(id)
	}))
}

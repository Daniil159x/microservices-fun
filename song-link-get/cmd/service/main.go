package main

import (
	"log"
	"song-link-get/webapi"
)

func main() {

	api := webapi.New()
	api.RegisterRoutes()
	log.Fatal(api.ListenAndServe("0.0.0.0", 5348))
}

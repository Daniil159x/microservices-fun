package webapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"song-link-get/utils"
)



type WebAPI struct {
}

func New() *WebAPI {
	return &WebAPI{}
}

func (api *WebAPI) RegisterRoutes() {
	http.HandleFunc("/api/track", api.getLinkFromSpotifyURI)
	http.HandleFunc("/api/all-links", api.getAllLinks)
}

func (api *WebAPI) ListenAndServe(ip string, port int) error {
	addr := fmt.Sprintf("%s:%d", ip, port)
	fmt.Printf("Start server on '%s'. API is awailable at http://%s/api\n", addr, addr)
	return http.ListenAndServe(addr, nil)
}

func (api *WebAPI) getLinkFromSpotifyURI(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	if !q.Has("uri") {
		http.Error(w, "Argument 'uri' not found", http.StatusBadRequest)
		return
	}

	uri, err := utils.ParseSpotifyURI(q.Get("uri"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if uri.Resource != "track" {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write([]byte(fmt.Sprintf("song.link/s/%s", uri.ID)))
}

func (api *WebAPI) getAllLinks(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	if !q.Has("uri") {
		http.Error(w, "Argument 'uri' not found", http.StatusBadRequest)
		return
	}
	uri := q.Get("uri")
	if !utils.IsSpotifyURI(uri) {
		http.Error(w, "Uri is not spotify uri", http.StatusBadRequest)
		return
	}

	songLinkEndpoint := fmt.Sprintf("https://api.song.link/v1-alpha.1/links?url=%s", uri)
	resp, err := http.Get(songLinkEndpoint)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil || resp.StatusCode != http.StatusOK {
		http.Error(w, "Upstream service failure", http.StatusBadGateway)
		return
	}

	var links SongLinksResponse
	err = json.NewDecoder(resp.Body).Decode(&links)
	if err != nil {
		http.Error(w, "Deserialize error", http.StatusInternalServerError)
		return
	}

	data, err := json.Marshal(links)
	if err != nil {
		http.Error(w, "Deserialize error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

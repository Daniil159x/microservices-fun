package webapi

type SongLinksPlatform struct {
	EntityUniqueId     string `json:"entityUniqueId"`
	URL                string `json:"url"`
	NativeAppURIMobile string `json:"nativeAppUriMobile,omitempty"`
	NativeAppURIDesktop string `json:"nativeAppUriDesktop,omitempty"`
}

type SongLinksResponse struct {
	PageURL string `json:"pageUrl"`

	LinksByPlatform map[string]SongLinksPlatform `json:"linksByPlatform"`
}

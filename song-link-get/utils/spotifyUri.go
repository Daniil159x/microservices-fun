package utils

import (
	"fmt"
	"regexp"
)

const spotifyURIPattern = `spotify:(\w+):([\w]+)`

var spotifyURIRegex = regexp.MustCompile(spotifyURIPattern)

type SpotifyURI struct {
	Resource string
	ID       string
}

func ParseSpotifyURI(uri string) (SpotifyURI, error) {
	var res SpotifyURI

	mt := spotifyURIRegex.FindAllStringSubmatch(uri, -1)
	if len(mt) == 0 {
		return res, fmt.Errorf("'%s' is not valid uri", uri)
	}

	res.Resource = mt[0][1]
	res.ID = mt[0][2]

	return res, nil
}

func IsSpotifyURI(uri string) bool {
	return spotifyURIRegex.MatchString(uri)
}
